﻿using System;
using System.Text;

/// <summary>
    /// Свойства жениха
    /// </summary>
    [Flags]
    public enum GroomProperties
    {
        selfish = 1,
        shy = 2,
        weak = 4,
        rich = 8
    }

    /// <summary>
    /// Свойства невесты
    /// </summary>
    [Flags]
    public enum BrideProperties
    {
        cranky = 1,
        selfsufficient = 2,
        kind = 4,
        housewife = 8
    }

    internal class Program
    {
        private static readonly Random _random = new Random();

        private static Groom[] _grooms;
        private static Bride[] _brides;

        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            InitializeGroomsArray(out _grooms);
            InitializeBrideArray(out _brides);

            OutputGroomsArray(_grooms);
            OutputBridesArray(_brides);

            SortGroomsNBrides(ref _grooms, ref _brides);
            Console.WriteLine();

            OutputGroomsArray(_grooms);
            OutputBridesArray(_brides);

            CorrelateBroomsNBrides(ref _grooms, ref _brides);
        }

        private static void CorrelateBroomsNBrides(ref Groom[] grooms, ref Bride[] brides)
        {
            bool chosenArray = grooms.Length > brides.Length;

            if (chosenArray)
            {
                for (int i = 0; i < grooms.Length; i++)
                {
                    if (i >= brides.Length)
                    {
                        Console.WriteLine("Groom[" + grooms[i].GetName() + "] : No matching bride");
                        continue;
                    }

                    Console.WriteLine("Grom[" + grooms[i].GetName() + "] : Bride[" + brides[i].GetName() + "] : Match");
                }

                return;
            }

            for (int i = 0; i < brides.Length; i++)
            {
                if (i > grooms.Length)
                {
                    Console.WriteLine("Bride[" + grooms[i].GetName() + "] : No matching groom");
                    continue;
                }

                Console.WriteLine("Bride[" + brides[i].GetName() + "] : Groom[" + grooms[i].GetName() + "] : Match");
            }
        }

        /// <summary>
        /// Инициализация массива с женихами
        /// </summary>
        /// <param name="grooms"></param>
        private static void InitializeGroomsArray(out Groom[] grooms)
        {
            grooms = new Groom[_random.Next(2, 5)];
            for (var i = 0; i < grooms.Length; i++)
            {
                grooms[i] = new Groom();
                grooms[i].SetName(i.ToString());
                FillGroomWithProperties(ref grooms[i]);
            }
        }

        /// <summary>
        /// Инициализация массива с невестами
        /// </summary>
        /// <param name="brides"></param>
        private static void InitializeBrideArray(out Bride[] brides)
        {
            brides = new Bride[_random.Next(1, 5)];
            for (var i = 0; i < brides.Length; i++)
            {
                brides[i] = new Bride();
                brides[i].SetName(i.ToString());
                FillBrideWithProperties(ref brides[i]);
            }
        }

        /// <summary>
        /// Заполнение полей жениха
        /// </summary>
        /// <param name="groom"></param>
        private static void FillGroomWithProperties(ref Groom groom)
        {
            var propertysCounter = _random.Next(1, 4);
            for (var i = 0; i < propertysCounter; i++)
            {
                int propertyChose = _random.Next(6);
                var property = ChooseGroomProperty(propertyChose);
                if (CheckGroomProperty(property, groom))
                {
                    i--;
                    continue;
                }

                groom.Properties |= property;
            }
        }

        /// <summary>
        /// Заполнение полей невесты
        /// </summary>
        /// <param name="bride"></param>
        private static void FillBrideWithProperties(ref Bride bride)
        {
            var propertysCounter = _random.Next(1, 4);
            for (var i = 0; i < propertysCounter; i++)
            {
                int propertyChose = _random.Next(6);
                var property = ChooseBrideProperty(propertyChose);
                if (CheckBrideProperty(property, bride))
                {
                    i--;
                    continue;
                }

                bride.Properties |= property;
            }
        }

        /// <summary>
        /// Проверка на наличие дубликата свойства у невесты
        /// </summary>
        /// <param name="property"></param>
        /// <param name="bride"></param>
        /// <returns>True если такое свойство уже есть, false если нет</returns>
        private static bool CheckBrideProperty(BrideProperties property, Bride bride)
        {
            if ((bride.Properties & property) == property) return true;
            return false;
        }

        /// <summary>
        /// Проверка на наличие дубликата свойства у жениха
        /// </summary>
        /// <param name="property"></param>
        /// <param name="groom"></param>
        /// <returns>True если такое свойство уже есть, false если нет</returns>
        private static bool CheckGroomProperty(GroomProperties property, Groom groom)
        {
            if ((groom.Properties & property) == property) return true;
            return false;
        }

        /// <summary>
        /// Вывод в консоль массив невест вместе со свойствами
        /// </summary>
        /// <param name="brides"></param>
        private static void OutputBridesArray(Bride[] brides)
        {
            for (var i = 0; i < brides.Length; i++)
            {
                Console.Write("Bride[" + brides[i].GetName() + "] : ");
                Console.WriteLine(brides[i].ReturnProperties() + " ; Rank:" + Rank(brides[i].Properties));
            }
        }

        /// <summary>
        /// Вывод массива женихов вместе со свойствами
        /// </summary>
        /// <param name="grooms"></param>
        private static void OutputGroomsArray(Groom[] grooms)
        {
            for (var i = 0; i < grooms.Length; i++)
            {
                Console.Write("Groom[" + grooms[i].GetName() + "] : ");
                Console.WriteLine(grooms[i].ReturnProperties() + " ; Rank: " + Rank(grooms[i].Properties));
            }
        }

        /// <summary>
        /// Определить рейтинг жениха
        /// </summary>
        /// <param name="properties"></param>
        /// <returns>Рейтинг жениха</returns>
        private static int Rank(GroomProperties properties)
        {
            int rank = 0;

            if ((properties & GroomProperties.rich) == GroomProperties.rich)
            {
                rank += (int) GroomProperties.rich;
            }

            if ((properties & GroomProperties.weak) == GroomProperties.weak)
            {
                rank += (int) GroomProperties.weak;
            }

            if ((properties & GroomProperties.shy) == GroomProperties.shy)
            {
                rank += (int) GroomProperties.shy;
            }

            if ((properties & GroomProperties.selfish) == GroomProperties.selfish)
            {
                rank += (int) GroomProperties.selfish;
            }


            return rank;
        }

        /// <summary>
        /// Определить рейтинг невесты
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static int Rank(BrideProperties properties)
        {
            int rank = 0;

            if ((properties & BrideProperties.housewife) == BrideProperties.housewife)
            {
                rank += (int) BrideProperties.housewife;
            }

            if ((properties & BrideProperties.kind) == BrideProperties.kind)
            {
                rank += (int) BrideProperties.kind;
            }

            if ((properties & BrideProperties.selfsufficient) == BrideProperties.selfsufficient)
            {
                rank += (int) BrideProperties.selfsufficient;
            }

            if ((properties & BrideProperties.cranky) == BrideProperties.cranky)
            {
                rank += (int) BrideProperties.cranky;
            }

            return rank;
        }

        /// <summary>
        /// Выбрать одно из 4 свойств жениха
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private static GroomProperties ChooseGroomProperty(int property)
        {
            switch (property)
            {
                case 1:
                    return GroomProperties.selfish;
                case 2:
                    return GroomProperties.shy;
                case 3:
                    return GroomProperties.weak;
                case 4:
                    return GroomProperties.rich;
            }

            return GroomProperties.selfish;
        }

        /// <summary>
        /// Выбрать одно из 4 свойств невесты
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private static BrideProperties ChooseBrideProperty(int property)
        {
            switch (property)
            {
                case 1:
                    return BrideProperties.cranky;
                case 2:
                    return BrideProperties.selfsufficient;
                case 3:
                    return BrideProperties.kind;
                case 4:
                    return BrideProperties.housewife;
            }

            return BrideProperties.cranky;
        }

        /// <summary>
        /// @TODO Доделать соотношение по предпочтению
        /// </summary>
        /// <param name="grooms"></param>
        /// <param name="brides"></param>
        private static void SortGroomsNBrides(ref Groom[] grooms, ref Bride[] brides)
        {
            for (int i = 0; i < grooms.Length; i++)
            {
                for (int j = 0; j < grooms.Length - 1; j++)
                {
                    if (Rank(grooms[j].Properties) < Rank(grooms[j + 1].Properties))
                    {
                        SwapGrooms(ref grooms[j], ref grooms[j + 1]);
                    }
                }
            }

            for (int i = 0; i < brides.Length; i++)
            {
                for (int j = 0; j < brides.Length - 1; j++)
                {
                    if (Rank(brides[j].Properties) < Rank(brides[j + 1].Properties))
                    {
                        SwapBrides(ref brides[j], ref brides[j + 1]);
                    }
                }
            }
        }

        private static void SwapGrooms(ref Groom A, ref Groom B)
        {
            Groom tempGroom = new Groom(A);
            A = B;
            B = tempGroom;
        }

        private static void SwapBrides(ref Bride A, ref Bride B)
        {
            Bride tempGroom = new Bride(A);
            A = B;
            B = tempGroom;
        }
    }

    /// <summary>
    /// Класс жениха
    /// </summary>
    internal class Groom
    {
        private string _name;
        public GroomProperties Properties;

        public Groom()
        {
            _name = "";
        }

        public Groom(Groom copyGroom)
        {
            _name = copyGroom._name;
            Properties = copyGroom.Properties;
        }

        public void SetName(string inputName)
        {
            _name = inputName;
        }

        public string GetName()
        {
            return _name;
        }

        public string ReturnProperties()
        {
            return string.Format(Properties.ToString());
        }
    }

    /// <summary>
    /// Класс невесты
    /// </summary>
    internal class Bride
    {
        private string _name;
        public BrideProperties Properties;

        public Bride()
        {
            _name = "";
        }

        public Bride(Bride copyBride)
        {
            _name = copyBride._name;
            Properties = copyBride.Properties;
        }

        public void SetName(string inputName)
        {
            _name = inputName;
        }

        public string GetName()
        {
            return _name;
        }

        public string ReturnProperties()
        {
            return string.Format(Properties.ToString());
        }
    }
    