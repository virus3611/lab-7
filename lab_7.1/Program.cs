﻿using System;

namespace lab_7._1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int x = 0;
            double eps = 0.0; 
            int n = 0;
            x = int.Parse(Console.ReadLine()); 
            n = int.Parse(Console.ReadLine());
            eps = double.Parse(Console.ReadLine());
            Console.WriteLine(myLn(x,eps,n));
            
        }
        public static double myLn(double x, double Eps, int n)
        {
            double rezult = 0.0;
            double midValue = 0.0;
            n = 0;
            do
            {
                midValue = 1 / ((2*n+1)*Math.Pow(x,2*n+1));
                n++;
                rezult += midValue;
            } while (Math.Abs(midValue)>Eps);
            return rezult * 2;
        }
    }
}